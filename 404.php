<?php get_template_part( 'includes/global/header' ); ?>

<main role="main" id="content">
  <div class="error__wrap">
    <h2 class="error-title"><?php the_field('404_page_title', 'option'); ?></h2>
    <div class="error-text"><?php the_field('404_page_content', 'option'); ?></div>
  </div>
</main>

<?php get_template_part( 'includes/global/footer' ); ?>
