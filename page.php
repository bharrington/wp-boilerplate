<?php get_template_part( 'includes/global/header' ); ?>

<main id="content">
  <?php while ( have_posts() ) : the_post(); ?>
    <?php the_content(); ?>
  <?php endwhile; ?>
</main>

<?php get_template_part( 'includes/global/footer' ); ?>
