<?php
// Theming Functions
require_once ('functions/theme/theme-assets-enqueue.php');
require_once ('functions/theme/theme-support.php');
require_once ('functions/theme/theme-sitemap.php');

// Basic Functions
require_once ('functions/basic/basic-security.php');

// Admin Functions
require_once ('functions/admin/admin-cleanup.php');
require_once ('functions/admin/admin-columns.php');
require_once ('functions/admin/admin-editor-style.php');
require_once ('functions/admin/admin-functions.php');
require_once ('functions/admin/admin-menu-classes.php');
require_once ('functions/admin/admin-menu-hide.php');
require_once ('functions/admin/admin-no-theme-edit.php');
require_once ('functions/admin/admin-panel.php');
require_once ('functions/admin/admin-style.php');

// Customizations
require_once ('functions/custom/custom-body-class.php');
require_once ('functions/custom/custom-fields.php');
require_once ('functions/custom/custom-image-sizes.php');
require_once ('functions/custom/custom-menus.php');
require_once ('functions/custom/custom-posts.php');
require_once ('functions/custom/custom-tinymce-base-edit.php');
require_once ('functions/custom/custom-tinymce-items.php');
require_once ('functions/custom/custom-functions.php');

// Blocks
require_once ('functions/custom-blocks/blocks-settings.php');
require_once ('functions/custom-blocks/blocks.php');
