<?php
// Theme styles for block editor
add_action( 'after_setup_theme', 'bph_custom_editor_styles' );
function bph_custom_editor_styles() {
  add_theme_support( 'editor-styles' );
  add_editor_style( 'dist/css/styles.css' );
}

// Admin style
add_action( 'admin_enqueue_scripts', 'bph_custom_admin_styles' );
function bph_custom_admin_styles() {
  wp_enqueue_style( 'admin-styles', get_template_directory_uri() . '/dist/css/admin.css', array(), filemtime(get_stylesheet_directory() . '/dist/css/admin.css') );
}
