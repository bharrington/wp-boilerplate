<?php
/////////////////
// example
/////////////////
add_filter("manage_edit-name_columns", "bph_name_columns");
function bph_name_columns($columns) {
  $columns = array(
    'cb'        => '<input type="checkbox" />',
    'photo'     => '',
    'title'     => 'Title',
  );
  return $columns;
}

add_action("manage_name_posts_custom_column", "bph_name_custom_columns", 10, 2);
function bph_name_custom_columns($column, $post_id) {
  global $post;
  if($column == 'photo') {
    $name_photo  = get_field('image');
    $name_photo_ = $name_photo['sizes']['bph_sm'];

    if ( $name_photo_ ) {
      echo '<img src="' . $name_photo_ .'">';
    } else {
      echo '<i class="fas fa-user-circle"></i>';
    }
  }
}

// name sortable
add_filter("manage_edit-name_sortable_columns", "bph_name_column_sortable" );
function bph_name_column_sortable( $columns ) {
  $columns['photo'] = 'photo';
  return $columns;
}


/////////////////
// page
/////////////////
add_filter("manage_edit-page_columns", "bph_page_columns");
function bph_page_columns($columns) {
  $columns = array(
    'cb'       => '<input type="checkbox" />',
    'title'    => 'Title',
    'template' => 'Template',
    'author'   => 'Author',
    'date'     => 'Date',
  );
  return $columns;
}

add_action("manage_page_posts_custom_column", "bph_page_custom_columns", 10, 2);
function bph_page_custom_columns($column, $post_id) {
  global $post;
  $template_file = get_post_meta( get_the_ID(), '_wp_page_template', TRUE );
  $template_file_ = str_replace(".php", "", $template_file);
  $template_file__ = str_replace("-", " ", $template_file_);

  if($column == 'template') {
    echo '<p>' . basename($template_file__) . '</p>';
  }
}


