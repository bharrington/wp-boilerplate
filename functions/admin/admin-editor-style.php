<?php
add_action( 'init', 'bph_add_editor_styles' );
function bph_add_editor_styles() {
  add_editor_style( get_template_directory_uri() . '/dist/css/style.css' );
}

// Add style to mce
add_filter( 'tiny_mce_before_init', 'bph_mce_style' );
function bph_mce_style( $mceInit ) {
  $custom_css = get_theme_mod( 'custom_css' );
  $styles = '.mce-content-body { padding: 35px ' . $custom_css . '; }';

  if ( !isset( $mceInit['content_style'] ) ) {
    $mceInit['content_style'] = $styles . ' ';
  }
  else {
    $mceInit['content_style'] .= ' ' . $styles . ' ';
  }
  return $mceInit;
}
