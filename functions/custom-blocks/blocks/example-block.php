<?php
add_action('acf/init', 'bph_block_example');
function bph_block_example() {

  if( function_exists('acf_register_block') ) {
    acf_register_block(array(
      'name'        => 'example-block',
      'title'       => __('Example ACF Block'),
      'description'   => __('This is an example ACF Block'),
      'render_template'   => get_template_directory() . '/includes/blocks/example-block/example-block.php',
      'enqueue_script'    => get_template_directory_uri() . '/includes/blocks/example-block/example-block.js',
      'category'      => 'ftt-blocks',
      'icon'        => '',
      'keywords'      => array( 'example'),
    ));
  }
}

add_action('after_setup_theme', 'bph_block_example_editor_styles');
function bph_block_example_editor_styles() {
  add_theme_support( 'editor-styles' );
  add_editor_style( 'includes/blocks/example-block/example-block-editor.css' );
}
