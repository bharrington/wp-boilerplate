<?php
// Custom FTT category
add_filter( 'block_categories', 'bph_block_category', 10, 2);
function bph_block_category( $categories, $post ) {
  return array_merge(
    $categories,
    array(
      array(
        'slug' => 'ftt-blocks',
        'title' => __( 'Blocks', 'ftt-blocks' ),
      ),
    )
  );
}

// Add ACF registered blocks here
add_filter( 'allowed_block_types', 'bph_allowed_block_types', 10, 2 );
function bph_allowed_block_types( $allowed_blocks, $post ) {
 $allowed_blocks = array(
      'core/block',
      'acf/example-block'
  );
  return $allowed_blocks;
}

