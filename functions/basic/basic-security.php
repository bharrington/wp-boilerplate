<?php
// remove version info from head and feeds
add_filter('the_generator', 'bph_complete_version_removal');
function bph_complete_version_removal() {
  return '';
}

//remove pings to self
add_action( 'pre_ping', 'bph_no_self_ping' );
function bph_no_self_ping( &$links ) {
  $home = get_option( 'home' );
  foreach( $links as $l => $link )
      if( 0 === strpos( $link, $home ))
        unset($links[$l]);
}

// no error message on login failure
add_filter( 'login_errors', 'bph_login_errors' );
function bph_login_errors(){
  return 'Something is wrong!';
}

// prevent multisite signup
add_action( 'signup_header', 'bph_prevent_multisite_signup' );
function bph_prevent_multisite_signup() {
  wp_redirect( site_url() );
  die();
}

// Disable ping back scanner and complete xmlrpc class.
add_filter( 'wp_xmlrpc_server_class', '__return_false' );
add_filter('xmlrpc_enabled', '__return_false');

//remove xpingback header
add_filter('wp_headers', 'bph_remove_x_pingback');
function bph_remove_x_pingback($headers) {
  unset($headers['X-Pingback']);
  return $headers;
}
