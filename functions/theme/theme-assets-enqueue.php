<?php
// Remove jquery
if( !is_admin()){
  add_action('wp_enqueue_scripts', 'bph_no_jquery');
  function bph_no_jquery() {
    wp_deregister_script('jquery');
  }
}

// Load theme & block css
add_action( 'enqueue_block_editor_assets', 'bph_theme_css' );
add_action( 'wp_enqueue_scripts', 'bph_theme_css' );

function bph_theme_css() {
  wp_enqueue_style( 'style-name', get_template_directory_uri() . '/dist/css/styles.css', array(), filemtime(get_stylesheet_directory() . '/dist/css/styles.css') );
}

// Load vendor js
add_action( 'wp_enqueue_scripts', 'bph_theme_js_vendor' );
function bph_theme_js_vendor() {

}

// Load theme js & css
add_action( 'wp_enqueue_scripts', 'bph_theme_js_css' );
add_action( 'enqueue_block_editor_assets', 'bph_theme_js_css' );
function bph_theme_js_css() {
  wp_enqueue_style( 'styles', get_template_directory_uri() . '/dist/css/styles.css', array(), filemtime(get_stylesheet_directory() . '/dist/css/styles.css') );
  wp_enqueue_script( 'application', get_template_directory_uri() . '/dist/js/app.js', array(), filemtime(get_stylesheet_directory() . '/dist/js/app.js'), true );
}

// Unload gutenburg css
add_action( 'wp_enqueue_scripts', 'bph_remove_block_css', 100 );
function bph_remove_block_css(){
  wp_dequeue_style( 'wp-block-library' );
}

// Unload mce css
add_filter("mce_css", "ftt_remove_mce_css");
function ftt_remove_mce_css($css) {
  $css = explode(',',$css);
  foreach ($css as $key => $sheet) {
    if (preg_match('/wp\-includes/',$sheet)) {
      unset($css[$key]);
    }
  }
  $css = implode(',',$css);
  return $css;
}
