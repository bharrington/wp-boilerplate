<?php
function bph_create_sitemap() {
  $items = (array) get_posts(array(
    'numberposts' => -1,
    'post_type' => (array) array(
      (string) 'page',
    ),
    'post_status' => (string) 'publish',
  ));

  $sitemap = (string) '';
  $sitemap .= '<?xml version=\'1.0\' encoding=\'UTF-8\'?>';
  $sitemap .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
  foreach ($items as $item) {
    $sitemap .= '<url>';
    $sitemap .= '  <loc>'.get_permalink($item->ID).'</loc>';
    $sitemap .= '  <lastmod>'.get_the_modified_date('Y-m-d', $item->ID).'</lastmod>';
    $sitemap .= '  <changefreq>daily</changefreq>';
    $sitemap .= '  <priority>1</priority>';
    $sitemap .= '</url>';
  }
  $sitemap .= '</urlset>';

  file_put_contents(ABSPATH.'sitemap.xml', $sitemap);
}
add_action('save_post', 'bph_create_sitemap');
