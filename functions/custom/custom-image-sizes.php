<?php
// allow uploading of svgs to media library
function bph_custom_mime_types( $mimes ) {
  $mimes['svgxml'] = 'image/svg+xml';
  $mimes['svg'] = 'image/svg';
  return $mimes;
}
add_filter( 'upload_mimes', 'bph_custom_mime_types' );

// image sizes
function bph_custom_images() {
  // hero images
  add_image_size( 'bh_hero_sm', 600,  9999 );
  add_image_size( 'bh_hero_md', 1000, 9999 );
  add_image_size( 'bh_hero_lg', 1600, 9999 );
  add_image_size( 'bh_hero_xl', 1900, 9999 );

  // general image sizes
  add_image_size( 'bph_sm', 350,  9999 );
  add_image_size( 'bph_md', 700,  9999 );
  add_image_size( 'bph_lg', 1400, 9999 );

  // general icon sizes
  add_image_size( 'bph_icon', 175, 9999 );
}
add_action( 'after_setup_theme', 'bph_custom_images' );
