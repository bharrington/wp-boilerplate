<?php
if( function_exists('acf_add_options_page') ) {
  acf_add_options_page(array(
    'page_title'  => 'Customize',
    'menu_title'  => 'Customize',
    'menu_slug'   => 'customize',
    'capability'  => 'manage_options',
    'redirect'    => false
  ));
}
