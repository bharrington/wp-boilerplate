<?php
function bph_add_acf() {
    ?>
    <div class="notice notice-warning is-dismissible">
        <p><?php _e( 'Make sure to add the current ACF plugin code to theme.', 'bph' ); ?></p>
    </div>
    <?php
}
add_action( 'admin_notices', 'bph_add_acf' );
