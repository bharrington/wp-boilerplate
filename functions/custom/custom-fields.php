<?php
if (!file_exists(get_stylesheet_directory() . '/functions/custom/custom-fields/acf/acf.php')) {
  include_once('custom-fields/notice.php');
  return;
}

// load acf
include_once('custom-fields/acf/acf.php');

// Load an options page
include_once('custom-fields/options-init.php' );

// load style
add_filter('acf/settings/path', 'ftt_acf_settings_path');
function ftt_acf_settings_path( $path ) {
  $path = get_stylesheet_directory() . '/functions/custom/custom-fields/acf/';
  return $path;
}

// load settings
add_filter('acf/settings/dir', 'ftt_acf_settings_dir');
function ftt_acf_settings_dir( $dir ) {
  $dir = get_stylesheet_directory_uri() . '/functions/custom/custom-fields/acf/';
  return $dir;
}

// hide admin
if ( file_exists( get_stylesheet_directory() . '/_env-local' ) ) {
  // skip
} else {
  add_filter('acf/settings/show_admin', '__return_false');
}

// custom save point
add_filter('acf/settings/save_json', 'ftt_acf_json_save_point');
function ftt_acf_json_save_point( $path ) {
  // update path
  $path = get_stylesheet_directory() . '/functions/custom/custom-fields/json';
  // return
  return $path;
}

// custom load path
add_filter('acf/settings/load_json', 'ftt_acf_json_load_point');
function ftt_acf_json_load_point( $paths ) {
  // remove original path (optional)
  unset($paths[0]);
  // append path
  $paths[] = get_stylesheet_directory() . '/functions/custom/custom-fields/json';
  // return
  return $paths;
}
