<?php
/**
 * Block Name: Example
 *
 * This is the template that displays the example block.
 */

?>
<div class="example">
  <h1><?php the_field('title'); ?></h1>
</div>
