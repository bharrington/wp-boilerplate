<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <link rel="profile" href="https://gmpg.org/xfn/11" />

  <?php if( get_field('alt_title') ): ?>
    <title><?php bloginfo('name'); ?> - <?php the_field('alt_title'); ?></title>
  <?php else: ?>
    <title><?php bloginfo('name'); ?> - <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
  <?php endif; ?>

  <?php if( get_field('meta_description') ): ?>
    <meta name="description" content="<?php the_field('meta_description'); ?>"/>
  <?php endif; ?>

  <?php if ( '' != locate_template( 'includes/global/critical-css.php' )): ?>
    <style><?php get_template_part( 'includes/global/critical-css' ); ?></style>
  <?php endif; ?>
  <?php wp_head(); ?>
</head>

<?php $custom_class = 'custom-class'; ?>
<body <?php body_class("$custom_class"); ?> >
  <?php wp_body_open(); ?>
  <a class="skip-link" href="#content"><?php _e( 'Skip to content', 'ftt' ); ?></a>

  <header role="banner" class="header">
    <a href="<?php echo home_url(); ?>">
      <h1 class="header__logo"><?php bloginfo('name'); ?></h1>
    </a>

    <i class="menu__trigger" tabindex="0">Menu</i>
  </header>

  <nav role="navigation" aria-label="<?php _e( 'Main navigation', 'ftt' ); ?>" class="menu menu--header">
    <?php bem_menu('menu__header', 'menu', '', '2'); ?>
  </nav>
